package bai12;

import java.time.Year;

public class XeMay extends PhuongTien {
    private double congSuat;

    public XeMay(String hangSanXuat, Year namSanXuat, double giaBan, String mauXe) {
        super(hangSanXuat, namSanXuat, giaBan, mauXe);
    }

    public XeMay(String hangSanXuat, Year namSanXuat, double giaBan, String mauXe, double congSuat) {
        super(hangSanXuat, namSanXuat, giaBan, mauXe);
        this.congSuat = congSuat;
    }

    @Override
    public String toString() {
        return super.toString() + "bai13.XeMay{" +
                "congSuat=" + congSuat +
                '}';
    }
}
