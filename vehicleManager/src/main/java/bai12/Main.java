package bai12;

import java.time.Year;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        QLPTGT qlptgt = new QLPTGT();
        PhuongTien oTo = new OTo("Oto", Year.parse("2019"), 1000_000_000, "màu đen", 6, null);
        PhuongTien xemay = new XeMay("Xemay", Year.parse("2018"), 60_000_000, "màu đen", 120);
        PhuongTien xetai = new XeTai("Xemay", Year.parse("2018"), 60_000_000, "màu đen", 120);

        PhuongTien oTo2 = new OTo("Oto2", Year.parse("2019"), 1000_000_000, "màu đen", 6, null);
        PhuongTien xemay2 = new XeMay("Xemay2", Year.parse("2018"), 60_000_000, "màu đen", 120);
        PhuongTien xetai2 = new XeTai("Xemay2", Year.parse("2018"), 60_000_000, "màu xanh", 120);

        PhuongTien oTo3 = new OTo("Oto3", Year.parse("2019"), 1000_000_000, "màu xanh", 6, null);
        PhuongTien xemay3 = new XeMay("Xemay3", Year.parse("2018"), 60_000_000, null, 120);
        PhuongTien xetai3 = new XeTai("Xemay3", Year.parse("2018"), 60_000_000, "màu đen", 120);

        qlptgt.addPhuongTien(oTo).addPhuongTien(xemay).addPhuongTien(xetai);

        qlptgt.addPhuongTien(oTo2).addPhuongTien(xemay2).addPhuongTien(xetai2);

        qlptgt.addPhuongTien(oTo3);
        qlptgt.addPhuongTien(xemay3);
        qlptgt.addPhuongTien(xetai3);

        qlptgt.xuatTatCaLoaiPhuongTien();

//        qlptgt.removePhuongTien(oTo3);
//
//        System.out.println("Sau khi remove");
//        qlptgt.xuatTatCaLoaiPhuongTien();

        List<PhuongTien> chiCoMauXanh = qlptgt.searchPhuongTien("Xanh");
        System.out.println("Thực hiện search");
        qlptgt.xuatTatCaLoaiPhuongTien(chiCoMauXanh);
    }
}
