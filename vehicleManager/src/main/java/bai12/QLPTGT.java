package bai12;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class QLPTGT {
    private List<PhuongTien> phuongTienList = new ArrayList<>();

    public QLPTGT addPhuongTien(PhuongTien phuongTien) {
        phuongTienList.add(phuongTien);
        return this;
    }

    public void removePhuongTien(PhuongTien phuongTien) {
        phuongTienList.remove(phuongTien);
    }

    public List<PhuongTien> searchPhuongTien(String mau) {
        List<PhuongTien> chicoMau = new ArrayList<>();
        for(PhuongTien phuongTien : phuongTienList) {
//            if(mau.equals(phuongTien.getMauXe())) {
//                chicoMau.add(phuongTien);
//            }
//            "Xanh" -> "xanh"
//            "Xanh" -> "XANH"
            if(Objects.nonNull(phuongTien.getMauXe()) && phuongTien.getMauXe().contains(mau.toLowerCase())) {
                chicoMau.add(phuongTien);
            }
        }
        return chicoMau;
    }

    public void setPhuongTienList(List<PhuongTien> phuongTienList) {
        this.phuongTienList = phuongTienList;
    }

    public void NhapLoaiPhuongTien() {

    }

    public void xuatTatCaLoaiPhuongTien() {
        xuatTatCaLoaiPhuongTien(phuongTienList);
    }

    public void xuatTatCaLoaiPhuongTien(List<PhuongTien> phuongTienList) {
        for (PhuongTien phuongTien : phuongTienList) {
            System.out.println(phuongTien);
        }
    }
}
