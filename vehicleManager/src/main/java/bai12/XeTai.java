package bai12;

import java.time.Year;

public class XeTai extends PhuongTien {
    private double trongTai;

    public XeTai(String hangSanXuat, Year namSanXuat, double giaBan, String mauXe, double trongTai) {
        super(hangSanXuat, namSanXuat, giaBan, mauXe);
        this.trongTai = trongTai;
    }

    @Override
    public String toString() {
        return super.toString() + "bai13.XeTai{" +
                "trongTai=" + trongTai +
                '}';
    }
}
