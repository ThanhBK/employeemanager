package bai12;

import java.time.Year;

public class OTo extends PhuongTien {
    private int soChoNgoi;
    private String kieuDongCo;

    public OTo(String hangSanXuat, Year namSanXuat, double giaBan, String mauXe, int soChoNgoi, String kieuDongCo) {
        super(hangSanXuat, namSanXuat, giaBan, mauXe);
        this.soChoNgoi = soChoNgoi;
        this.kieuDongCo = kieuDongCo;
    }

    @Override
    public String toString() {
        return super.toString() + "bai13.OTo{" +
                "soChoNgoi=" + soChoNgoi +
                ", kieuDongCo='" + kieuDongCo + '\'' +
                '}';
    }
}
