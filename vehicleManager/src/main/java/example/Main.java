package example;

import example.entity.Custormer;
import example.entity.Gender;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("example");
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static void main(String[] args) {
        LocalDate birthDay = LocalDate.parse("2014-03-04", DATE_TIME_FORMATTER);
        String[] phones = new String[]{"123456789", "2345678901"};
        Custormer custormer1 = new Custormer("bùi viết", "thành", "buivietthanh5@gmail.com", phones, birthDay, Gender.MALE);
        Custormer custormer2 = new Custormer("bùi viết", "thành", "buivietthanh6@gmail.com", phones, birthDay, Gender.MALE);

        System.out.println("add customer");
        addCustomer(custormer1);
        addCustomer(custormer2);

        System.out.println("show customers");
        List<Custormer> custormers = getCustomers();
        custormers.forEach(custormer -> System.out.println(custormer));

        System.out.println("get customer by id");
        System.out.println(getCustomer(100));
        ENTITY_MANAGER_FACTORY.close();
    }

    public static void addCustomer(Custormer newCustomer) {
        EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction et = null;
        try {
            et = entityManager.getTransaction();
            et.begin();
            entityManager.persist(newCustomer);
            et.commit();
        } catch (Exception ex) {
            if (et != null) {
                et.rollback();
            }
            ex.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    public static Custormer getCustomer(int id) {
        EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        Custormer custormer = null;
        try {
            custormer = entityManager.find(Custormer.class, id);
        } catch (NoResultException ex) {
            ex.printStackTrace();
        } finally {
            entityManager.close();
        }
        return custormer;
    }

    public static List<Custormer> getCustomers() {
        EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        String query = "SELECT c FROM Custormer c";
        TypedQuery<Custormer> typedQuery = entityManager.createQuery(query, Custormer.class);
        List<Custormer> custormers = new ArrayList<>();
        try {
            custormers = typedQuery.getResultList();
        } catch (NoResultException ex) {
            ex.printStackTrace();
        } finally {
            entityManager.close();
        }
        return custormers;
    }
}
