package example.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "customer")
@Data
@NoArgsConstructor
public class Custormer {
    @Id
    @Column(name = "custID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name = "firstName", nullable = true)
    private String fName;

    @Column(name = "lastName", nullable = true)
    private String lName;


    @Column(name = "email", nullable = true, unique = true)
    private String email;

    private String[] phones;

    private LocalDate birthday;
    @Enumerated(EnumType.STRING)
    private Gender gender;


    public Custormer(String fName, String lName, String email, String[] phones, LocalDate birthday, Gender gender) {
        this.fName = fName;
        this.lName = lName;
        this.email = email;
        this.phones = phones;
        this.birthday = birthday;
        this.gender = gender;
    }
}
