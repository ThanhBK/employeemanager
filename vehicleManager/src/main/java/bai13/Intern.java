package bai13;

import java.time.LocalDate;

public class Intern extends Employee {
    private String majors;

    private int semester;

    private String university_name;

    public Intern(int id, String fullName, LocalDate birthDay, String phone, String email, int employee_Type, String majors, int semester, String university_name) {
        super(id, fullName, birthDay, phone, email, employee_Type);
        this.majors = majors;
        this.semester = semester;
        this.university_name = university_name;
    }

    @Override
    public void showInfor() {
        super.showInfor();
        System.out.println("Majors :" + majors);
        System.out.println("semester :" + semester);
        System.out.println("university_name :" + university_name);
    }
}
