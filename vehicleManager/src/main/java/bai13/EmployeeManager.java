package bai13;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

public class EmployeeManager {
    private List<Employee> employees = new ArrayList<>();

    public EmployeeManager() {
    }

    public EmployeeManager(List<Employee> employees) {
        this.employees = employees;
    }

    public EmployeeManager addEmployee(Employee employee) {
        if (!isSearchEmployeeById(employee.getId())) {
            employees.add(employee);
        }
        return this;
    }

    public EmployeeManager editEmployee(int id, Employee newEmployee) {
        if (isSearchEmployeeById(id)) {
            Employee employee = searchEmployeeById(id);
            employee.setId(id);
            employee.setFullName(newEmployee.getFullName());
            employee.setEmail(newEmployee.getEmail());
            if(Objects.nonNull(newEmployee.getPhone())) {
                employee.setPhone(newEmployee.getPhone());
            }
            employee.setBirthDay(newEmployee.getBirthDay());
            employee.setEmployee_Type(newEmployee.getEmployee_Type().getValue());
        }
        return this;
    }

    public Employee searchEmployeeById(int id) {
        for (Employee employee : employees) {
            if (employee.getId() == id) {
                return employee;
            }
        }
        return null;
    }

    public boolean isSearchEmployeeById(int id) {
        Employee employee = searchEmployeeById(id);
        return (Objects.isNull(employee)) ? false : true;
    }

    public EmployeeManager removeEmployee(int id) {
        if (isSearchEmployeeById(id)) {
            Employee employee = searchEmployeeById(id);
            employees.remove(employee);
        }
        return this;
    }

    public void showEmployeeAll() {
        for (Employee employee : employees) {
            System.out.println("");
            employee.showInfor();
        }
    }

    public static Map<String, Object> inputEmployeeData() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập id");
        int id = Integer.parseInt(scanner.nextLine());

        System.out.println("Nhập fullName");
        String fullName = scanner.nextLine();

        System.out.println("Nhập birthDay");
        LocalDate birthDay = LocalDate.parse(scanner.nextLine(), DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        System.out.println("Nhập email");
        String email = scanner.nextLine();

        System.out.println("Nhập phone");
        String phone = scanner.nextLine();

        System.out.println("Nhập employee_Type");
        int employee_Type = Integer.parseInt(scanner.nextLine());

        System.out.println("Nhập expInYear");
        float expInYear = Float.parseFloat(scanner.nextLine());

        System.out.println("Nhập proSkill");
        String proSkill = scanner.nextLine();

        System.out.println("Nhập Graduation_date");
        LocalDate graduation_date = LocalDate.parse(scanner.nextLine(), DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        System.out.println("Nhập graduation_rank");
        String graduation_rank = scanner.nextLine();

        System.out.println("Nhập education");
        String education = scanner.nextLine();

        System.out.println("Nhập majors");
        String majors = scanner.nextLine();

        System.out.println("Nhập university_name");
        String university_name = scanner.nextLine();

        System.out.println("Nhập semester");
        int semester = Integer.parseInt(scanner.nextLine());

        Map<String, Object> request = new HashMap<>();
        request.put("id", id);
        request.put("fullName", fullName);
        request.put("birthDay", birthDay);
        request.put("email", email);
        request.put("phone", phone);
        request.put("employee_Type", employee_Type);
        request.put("expInYear", expInYear);
        request.put("proSkill", proSkill);
        request.put("graduation_date", graduation_date);
        request.put("graduation_rank", graduation_rank);
        request.put("education", education);
        request.put("majors", majors);
        request.put("university_name", university_name);
        request.put("semester", semester);
        return request;
    }
}
