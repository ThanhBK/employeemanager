package bai13;

import java.time.LocalDate;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class Employee implements Cloneable {
    private int id;
    private String fullName;
    private LocalDate birthDay;
    private String phone;
    private String email;
    private int employee_Type;
    private static int employee_count;
    private List<Certificate> certificates;

    public Employee(int id, String fullName, LocalDate birthDay, String phone, String email, int employee_Type) {
        this.id = id;
        this.fullName = fullName;
        this.birthDay = birthDay;
        setPhone(phone);
        setEmail(email);
        this.employee_Type = employee_Type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    public String getPhone() {
        return phone;
    }

    // "sssss445567"
    public void setPhone(String phone) {
        String phoneNumberPattern = "(\\d-)?(\\d{3}-)?\\d{3}-\\d{4}";
        Pattern pattern = Pattern.compile(phoneNumberPattern);
        Matcher matcher = pattern.matcher(phone);
        if (matcher.find()) {
            this.phone = phone;
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        String emailPattern = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(emailPattern);
        Matcher matcher = pattern.matcher(email);
        if (matcher.find()) {
            this.email = email;
        }
    }

    public Employee_Type getEmployee_Type() {
        return Employee_Type.getValue(this.employee_Type);
    }

    public void setEmployee_Type(int employee_Type) {
        if (employee_Type >= 0 && employee_Type <= 2) {
            this.employee_Type = employee_Type;
        }
    }

    public static int getEmployee_count() {
        return employee_count;
    }

    public static void setEmployee_count(int employee_count) {
        Employee.employee_count = employee_count;
    }

    public List<Certificate> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<Certificate> certificates) {
        this.certificates = certificates;
    }

    public void showInfor() {
        System.out.println("ID :" + id);
        System.out.println("fullName :" + fullName);
        System.out.println("birthDay :" + birthDay);
        System.out.println("phone :" + phone);
        System.out.println("email :" + email);
        System.out.println("employee_Type :" + getEmployee_Type());
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
