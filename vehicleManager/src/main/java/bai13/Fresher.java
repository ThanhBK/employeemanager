package bai13;

import java.time.LocalDate;

public class Fresher extends Employee {
    private LocalDate Graduation_date;
    private String graduation_rank;
    private String education;

    public Fresher(int id, String fullName, LocalDate birthDay, String phone, String email, int employee_Type, LocalDate graduation_date, String graduation_rank, String education) {
        super(id, fullName, birthDay, phone, email, employee_Type);
        Graduation_date = graduation_date;
        this.graduation_rank = graduation_rank;
        this.education = education;
    }

    @Override
    public void showInfor() {
        super.showInfor();
        System.out.println("Graduation_date :" + Graduation_date);
        System.out.println("graduation_rank :" + graduation_rank);
        System.out.println("education :" + education);
    }
}
