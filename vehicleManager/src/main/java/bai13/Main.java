package bai13;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Scanner;

public class Main {
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    public static void main(String[] args) throws CloneNotSupportedException {
//        EmployeeManager employeeManager = new EmployeeManager();
//        Employee experience = new Experience(1, "nguyễn văn a", LocalDate.parse("10-07-2021", dateTimeFormatter), "12345678", "nguyenvana@gmail", 0, 1, null);
//        Employee experience2 = new Experience(2, "nguyễn văn b", LocalDate.parse("10-08-2021", dateTimeFormatter), "123456789", "nguyenvana@gmail.com", 0, 1, null);
//        Employee experience3 = new Experience(3, "nguyễn văn ac", LocalDate.parse("10-06-2021", dateTimeFormatter), "123456789", "nguyenvana@gmail.com", 0, 1, null);
//
//        Employee fresher = new Fresher(4, "nguyễn thị a", LocalDate.parse("10-07-2021", dateTimeFormatter), "123456789", "nguyenvana@gmail.com", 1, LocalDate.parse("22-06-2019", dateTimeFormatter), "2", "A");
//        Employee fresher2 = new Fresher(5, "nguyễn thị b", LocalDate.parse("10-08-2021", dateTimeFormatter), "123456789", "nguyenvana@gmail.com", 1, LocalDate.parse("22-06-2019", dateTimeFormatter), "2", "B");
//        Employee fresher3 = new Fresher(6, "nguyễn thị ac", LocalDate.parse("10-06-2021", dateTimeFormatter), "123456789", "nguyenvana@gmail.com", 1, LocalDate.parse("22-06-2019", dateTimeFormatter), "2", "C");
//
//        Employee intern = new Intern(4, "nguyễn intern", LocalDate.parse("10-06-2021", dateTimeFormatter), "123456789", "nguyenvana@gmail.com", 1, "X", 2, "C");
//        employeeManager.addEmployee(experience)
//                .addEmployee(experience2)
//                .addEmployee(experience3)
//                .addEmployee(fresher)
//                .addEmployee(fresher2)
//                .addEmployee(fresher3)
//                .addEmployee(intern);
////        employeeManager.showEmployeeAll();


        // buổi 2
//        Map<String, Object> request = EmployeeManager.inputEmployeeData();
//        Employee employee = EmployeeFactory.getEmployee(request);
//
//        EmployeeManager employeeManager = new EmployeeManager();
//        employeeManager.addEmployee(employee);
//        employeeManager.showEmployeeAll();
        float a = 2.0f/3 + 5.0f/3;
// 1001001010100101010 + gần đúng
//        BigDecimal a = BigDecimal.valueOf(1.1);
        BigDecimal b = BigDecimal.valueOf(1.2);
        System.out.println(a);
    }
}
