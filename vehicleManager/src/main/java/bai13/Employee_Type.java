package bai13;

public enum Employee_Type {
    Experience(0), Fresher(1), Intern(2);
    private int value;
    Employee_Type(int value) {
        this.value = value;
    }

    public static Employee_Type getValue(int value) {
        for (Employee_Type d : Employee_Type.values()) {
            if (d.value == value) {
                return d;
            }
        }
        return null;
    }

    public int getValue() {
        return value;
    }
}
