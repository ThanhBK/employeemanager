package bai13;

import java.time.LocalDate;

public class Experience extends Employee {
    private float expInYear;
    private String proSkill;

    public Experience(int id, String fullName, LocalDate birthDay, String phone, String email, int employee_Type, float expInYear, String proSkill) {
        super(id, fullName, birthDay, phone, email, employee_Type);
        this.expInYear = expInYear;
        this.proSkill = proSkill;
    }

    @Override
    public void showInfor() {
       super.showInfor();
        System.out.println("ExpInYear :" + expInYear);
        System.out.println("ProSkill :" + proSkill);
    }
}
