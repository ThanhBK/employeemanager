package bai13;

import java.time.LocalDate;
import java.util.Map;

public class EmployeeFactory {
    private EmployeeFactory() {

    }

    // ngoài 0-2 khởi tạo
    public static Employee getEmployee(Map<String, Object> request) {
        int id = (int) request.get("id");
        String fullName = (String) request.get("fullName");
        LocalDate birthDay = (LocalDate) request.get("birthDay");
        String email = (String) request.get("email");
        String phone = (String) request.get("phone");
        int employee_Type = (int) request.get("employee_Type");
        float expInYear = (float) request.get("expInYear");
        String proSkill = (String) request.get("proSkill");
        LocalDate graduation_date = (LocalDate) request.get("graduation_date");
        String graduation_rank = (String) request.get("graduation_rank");
        String education = (String) request.get("education");
        String majors = (String) request.get("majors");
        String university_name = (String) request.get("university_name");
        int semester = (int) request.get("semester");

        switch (employee_Type) {
            case 0:
                return new Experience(id, fullName, birthDay, phone, email, employee_Type, expInYear, proSkill);
            case 1:
                return new Fresher(id, fullName, birthDay, phone, email, employee_Type, graduation_date, graduation_rank, education);
            case 2:
                return new Intern(id, fullName, birthDay, phone, email, employee_Type, majors, semester, university_name);
            default:
                return new Experience(id, fullName, birthDay, phone, email, employee_Type, expInYear, proSkill);
        }
    }
}
